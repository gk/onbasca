# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""Delete objects older than <days>."""
import datetime
import logging

from onbasca.onbasca import config, util
from onbasca.onbasca.management.commands.common_cmd import OnbascaCommand
from onbasca.onbasca.models import BwFile, Consensus, Relay

logger = logging.getLogger(__name__)
now = datetime.datetime.utcnow()


class Command(OnbascaCommand):
    help = __doc__

    def add_arguments(self, parser):
        super().add_arguments(parser)
        parser.add_argument(
            "-d",
            "--days",
            default=config.OLDEST_DATA_DAYS,
            type=int,
            help="How many days in the past data is considered old.",
        )

    def handle(self, *args, **options):
        super().handle(*args, **options)
        days = options.get("days", None)
        if days is None:  # Accept "0" days
            days = config.OLDEST_DATA_DAYS
        util.modify_logging(
            scan=False,
            generate=False,
            log_level=options.get("log_level", None),
        )
        oldest_datetime = now - datetime.timedelta(days=days)

        BwFile.objects.filter(_obj_created_at__lt=oldest_datetime).delete()
        Relay.objects.filter(_obj_created_at__lt=oldest_datetime).delete()
        Consensus.objects.filter(_obj_created_at__lt=oldest_datetime).delete()
        logger.info("Deleted objects older than {} days".format(days))
