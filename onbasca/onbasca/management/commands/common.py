# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import argparse
import logging
from pprint import pformat

import toml

from onbasca.onbasca import config

logger = logging.getLogger(__name__)


def add_arguments(parser):
    parser.add_argument(
        "-c",
        "--config-path",
        type=argparse.FileType("r"),
        required=False,
        help="TOML onbasca configuration file path.",
        default=config.CONFIG_PATH,
    )
    parser.add_argument(
        "-l",
        "--log-level",
        required=False,
        help="Log level.",
    )
    return parser


def handle_cmd(*args, **options):
    logger.debug("Default config: \n%s", pformat(config.__dict__))
    logger.info("Parsing configuration %s", options["config_path"].name)
    try:
        parsed_obj = toml.load(options["config_path"])
    except Exception as e:
        logger.warning("Failed to parse configuration: %s", e)
    else:
        logger.debug("Parsed configuration: \n%s", pformat(parsed_obj))
        if parsed_obj.get("default", None):
            config.update_with_dict(parsed_obj["default"])
        else:
            logger.warning("Config file missing 'default' section.")
    logger.debug("Config after parsing file: \n%s", pformat(config.__dict__))
    config.update_with_dict(options)
    logger.debug(
        "Config after parsing cli args: \n%s", pformat(config.__dict__)
    )
