# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from django.contrib import admin

from onbasca.onbasca.models.relaybw import RelayBw

from .links import (
    bwfile_change_link,
    relay_change_link,
    relaydesc_change_link,
    routerstatus_change_link,
)


@admin.register(RelayBw)
class RelayBwAdmin(admin.ModelAdmin):
    # pass
    list_display = (
        "fingerprint",
        "nickname",
        "measured_at",
        # "created_at",
        "updated_at",
        # Next should be used in the future, instead of fingerprint
        # "master_key_ed25519",
        "bw_mean",
        "_bw_filt",
        "_ratio_stream",
        "_ratio_filt",
        "_ratio",
        "_bw_scaled",
        "_bw_scaled_limited",
        "_bw_scaled_limited_rounded",
        "bw",
        "error_stream",
        "error_circ",
        # "error_misc",
        # "error_second_relay",
        # "error_destination",
        # "desc_bw_avg",
        # "desc_bw_obs_last",
        # "desc_bw_obs_mean",
        # "desc_bw_bur",
        # "relay___min_bandwidth",
        "consensus_bandwidth",
        # "relay___routerstatuses_bandwidth_mean",
        "consensus_bandwidth_is_unmeasured",
        # "relay_in_recent_recent_consensus_count",
        # "relay_recent_priority_list_count",
        # "relay_recent_measurement_attempt_count",
        # "relay_recent_measurement_failure_count",
        # "relay_recent_measurements_excluded_error_count",
        "success",
        "vote",
        "unmeasured",
        "under_min_report",
        "relay_link",
        "bwfile_link",
        # "circ_fail",
        "is_exit",
    )
    list_filter = [
        "vote",
        "bwfile",
        "bwfile__consensus",
    ]
    search_fields = ["nickname", "fingerprint"]

    def relay_link(self, obj):
        if obj.relay:
            return relay_change_link(obj.relay)
        return None

    relay_link.short_description = "Relay"

    def bwfile_link(self, obj):
        if obj.bwfile:
            return bwfile_change_link(obj.bwfile)
        return None

    relay_link.short_description = "BwFile"

    def relay_relaydesc_latest_change_link(self, obj):
        d = obj.relay_relaydesc_latest()
        if d:
            return relaydesc_change_link(d)
        return None

    relay_link.short_description = "Last descriptor"

    def relay_routerstatus_latest_change_link(self, obj):
        d = obj.relay_routerstatus_latest()
        if d:
            return routerstatus_change_link(d)
        return None

    relay_routerstatus_latest_change_link.short_description = (
        "Last router status"
    )
