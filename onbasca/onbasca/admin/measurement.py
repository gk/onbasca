# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from django.contrib import admin

from onbasca.onbasca.models.measurement import Measurement

from .links import consensus_change_link, relay_change_link


@admin.register(Measurement)
class MeasurementAdmin(admin.ModelAdmin):
    list_display = [
        "created_at",
        "queued_at",
        "attempted_at",
        "finished_at",
        "relay_link",
        "helper_link",
        "as_exit",
        "bandwidth",
        "error",
        "consensus",
        "webserver",
    ]
    search_fields = ["relay__fingerprint"]
    readonly_fields = ["created_at"]
    list_filter = [
        "as_exit",
        "created_at",
        "consensus",
    ]

    def consensus_link(self, obj):
        if obj.relay.consensuses.latest():
            return consensus_change_link(obj.relay.consensuses.latest())
        return None

    consensus_link.short_description = "Last consensus"

    def relay_link(self, obj):
        if obj.relay:
            return relay_change_link(obj.relay)
        return None

    relay_link.short_description = "Relay"

    def helper_link(self, obj):
        if obj.helper:
            return relay_change_link(obj.helper)
        return None

    helper_link.short_description = "Helper"
