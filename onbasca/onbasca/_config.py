# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""Singleton to set defaults, configuration file and arguments."""
# from django.conf import settings
import logging

from . import defaults

logger = logging.getLogger(__name__)


class Config:
    def __init__(self):
        for conf in dir(defaults):
            if conf.isupper():
                setattr(self, conf, getattr(defaults, conf))

    def update_with_dict(self, conf_dict):
        logger.debug("Updating config.")
        for key, value in conf_dict.items():
            # Update only when the value has been set
            if value is not None:
                if isinstance(value, dict):
                    value_dict = getattr(self, key, None)
                    if value_dict:
                        for subkey, subvalue in value.items():
                            value_dict[subkey] = subvalue
                        setattr(self, key.upper(), value_dict)
                else:
                    setattr(self, key.upper(), value)
