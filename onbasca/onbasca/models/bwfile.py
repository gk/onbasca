# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import datetime
import logging
import sys

from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from stem.descriptor.bandwidth_file import BandwidthFile

from onbasca import __version__
from onbasca.base.models.bwfile import (
    BWFILE_KEYS,
    BwFileBase,
    BwFileManagerBase,
)
from onbasca.onbasca import config, constants

from .consensus import Consensus
from .heartbeat import Heartbeat
from .measurement import Measurement
from .relay import Relay
from .relaybw import RelayBw
from .scanner import Scanner

logger = logging.getLogger(__name__)

BWFILE_VERSION = "1.5"


class BwFileManager(BwFileManagerBase):
    def from_bandwidth_file(self, bandwidth_file):
        if isinstance(bandwidth_file, BandwidthFile):
            header = bandwidth_file
        else:
            header = bandwidth_file.header
            # in case is stem bwfile
        kwargs = dict([(k, getattr(header, k, None)) for k in BWFILE_KEYS])
        if isinstance(kwargs["timestamp"], int):
            kwargs["timestamp"] = datetime.datetime.utcfromtimestamp(
                kwargs["timestamp"]
            )

        if not kwargs["file_created"]:
            kwargs["file_created"] = kwargs.pop("timestamp")
        kwargs.pop("timestamp", None)

        valid_after = kwargs["file_created"].replace(minute=0, second=0)
        kwargs["consensus"], _ = Consensus.objects.get_or_create(
            valid_after=valid_after
        )
        bwfile, created = self.update_or_create(
            file_created=kwargs.pop("file_created"), defaults=kwargs
        )
        logger.info("Bwfile %s created: %s", bwfile, created)
        logger.info(
            "Parsing %s bandwidth lines", len(bandwidth_file.measurements)
        )
        for bwline in bandwidth_file.measurements.values():
            RelayBw.objects.from_bwfile_bwline(bwline, bwfile)
        return bwfile

    def generate(self):
        """

        https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/29710,
        https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40006,
        https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40023,
        https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40049,
        https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40017:
        Report relays and measurements using `config.RECENT_DAYS`
        (28 by default) days of measurements.

        """
        now = datetime.datetime.utcnow().replace(microsecond=0)
        # No need to filter objects by date, since the old ones are
        # automatically removed
        bwfile = BwFile()
        try:
            bwfile.consensus = Consensus.objects.latest()
        except ObjectDoesNotExist:
            logger.warning(
                "There is not any consensus. Have you run the scanner?"
            )
            sys.exit(1)
        bwfile.file_created = now

        # 1. Network averages
        bwfile.set_mu()
        bwfile.set_muf()
        # NOTE: In the case all measurements fail, mu and muf will be None
        if bwfile._mu and bwfile._muf and (bwfile._mu > bwfile._muf):
            logger.debug("Network mu greater than muf.")

        # 2. Relays' scaled bandwidth
        # Initialize relays
        logger.info(
            "Initializing RelayBws from %s Relays.", Relay.objects.count()
        )
        for relay in Relay.objects.all():
            relaybw = RelayBw.objects.from_relay(relay)
            relaybw.bwfile = bwfile
            relaybw.save()
            relaybw.set_vote()
        logger.info("Created %s RelayBws.", bwfile.relaybw_set.count())

        logger.info("Calculating Relays' scaled bandwidth.")
        for relaybw in bwfile.relaybw_set.exclude(vote=0):
            relaybw.set_bw_scaled()
        logger.info("Scaled Relays' bandwidth")
        bwfile.set_bw_sum()  # For statistics

        # 3. Network scaled
        bwfile.set_bw_scaled_sum()
        logger.info("Sum network scaled bandwidth: %s", bwfile._bw_scaled_sum)
        bwfile._limit = bwfile._bw_scaled_sum * 0.05
        logger.info("Network bandwidth limit.: %s", bwfile._limit)
        bwfile.save()

        # 4. Relays' scaled, limited and rounded
        logger.info("Calculating Relay's limited and rounded bandwidth.")
        # exclude _bw_scaled__isnull to avoid None * float operation
        for relaybw in bwfile.relaybw_set.exclude(vote=0).exclude(
            _bw_scaled__isnull=True
        ):
            relaybw.set_bw_scaled_limited()
            relaybw.set_bw_scaled_limited_rounded()
        logger.info("Calculated Relay's limited and rounded bandwidth.")

        # 5. Network sums, only for this bwfile
        bwfile.set_bw_scaled_limited_sum()
        bwfile.set_bw_scaled_limited_rounded_sum()
        bwfile.save()
        # Just to log.
        bwfile.is_consensus_weight_percent_diff_greater_maximum()

        # 6. Calculate all the keyvalues
        try:
            bwfile.earliest_bandwidth = (
                bwfile.relaybw_set.earliest().measured_at
            )
            bwfile.latest_bandwidth = (
                bwfile.relaybw_set.filter(measured_at__isnull=False)
                .latest()
                .measured_at
            )
        except ObjectDoesNotExist:
            logger.warning(
                "There are not any relays. Have you run the scanner first?"
            )
            sys.exit(1)
        bwfile.minimum_percent_eligible_relays = (
            constants.MIN_PERCT_RELAYS_REPORT
        )

        # There must be at least 1 consensus for the next lines.
        bwfile.number_consensus_relays = bwfile.consensus.relay_set.count()
        bwfile.minimum_number_eligible_relays = round(
            bwfile.number_consensus_relays
            * bwfile.minimum_percent_eligible_relays
            / 100
        )
        bwfile.number_eligible_relays = bwfile.eligible_relays()
        bwfile.percent_eligible_relays = round(
            100
            * bwfile.number_eligible_relays
            / bwfile.number_consensus_relays
        )

        # Assuming the scanner's hearbeat is the last.
        bwfile._heartbeat = Heartbeat.objects.latest()
        bwfile.save()
        bwfile.calculate_progress_to_report()

        bwfile.time_to_report_half_network = bwfile.estimated_time()

        # All recent consensuses (28 days by default)
        bwfile.recent_consensus_count = Consensus.objects.all().count()
        # Same as previous
        bwfile.recent_priority_list_count = bwfile.recent_consensus_count
        bwfile.recent_priority_relay_count = (
            Measurement.objects.values_list("relay__fingerprint")
            .filter(queued_at__isnull=False)
            .order_by("relay__fingerprint")
            .distinct("relay__fingerprint")
            .count()
        )
        bwfile.recent_measurement_attempt_count = (
            Measurement.objects.values_list("relay__fingerprint")
            .filter(attempted_at__isnull=False)
            .order_by("relay__fingerprint")
            .distinct("relay__fingerprint")
            .count()
        )
        bwfile.recent_measurement_failure_count = (
            Measurement.objects.values_list("relay__fingerprint")
            .filter(finished_at__isnull=False)
            .order_by("relay__fingerprint")
            .distinct("relay__fingerprint")
            .count()
        )
        bwfile.recent_measurements_excluded_error_count = (
            Measurement.objects.values_list("relay__fingerprint")
            .filter(error__isnull=False)
            .order_by("relay__fingerprint")
            .distinct("relay__fingerprint")
            .count()
        )

        bwfile.scanner_country = config.SCANNER_COUNTRY
        bwfile.destinations_countries = config.DESTINATIONS_COUNTRIES
        bwfile.software_version = __version__

        scanner = Scanner.load()
        bwfile.tor_version = scanner.tor_version
        bwfile.generator_started = scanner.started_at
        bwfile.save()
        return bwfile


class BwFile(BwFileBase):
    objects = BwFileManager()
    consensus = models.ForeignKey(
        "Consensus", null=True, blank=True, on_delete=models.CASCADE
    )
    destinations_countries = models.CharField(
        max_length=64,
        null=True,
        blank=True,
    )
    earliest_bandwidth = models.DateTimeField(null=True, blank=True)
    # from .scanner import Scanner
    generator_started = models.DateTimeField(null=True, blank=True)
    minimum_number_eligible_relays = models.PositiveSmallIntegerField(
        null=True, blank=True
    )
    minimum_percent_eligible_relays = models.PositiveSmallIntegerField(
        null=True, blank=True
    )
    number_consensus_relays = models.PositiveSmallIntegerField(
        null=True, blank=True
    )
    number_eligible_relays = models.PositiveSmallIntegerField(
        null=True, blank=True
    )
    percent_eligible_relays = models.PositiveSmallIntegerField(
        null=True, blank=True
    )

    recent_consensus_count = models.PositiveSmallIntegerField(
        null=True, blank=True
    )
    recent_priority_list_count = models.PositiveSmallIntegerField(
        null=True, blank=True
    )
    recent_priority_relay_count = models.PositiveSmallIntegerField(
        null=True, blank=True
    )
    recent_measurement_attempt_count = models.PositiveSmallIntegerField(
        null=True, blank=True
    )
    recent_measurement_failure_count = models.PositiveSmallIntegerField(
        null=True, blank=True
    )
    recent_measurements_excluded_error_count = (
        models.PositiveSmallIntegerField(null=True, blank=True)
    )
    recent_measurements_excluded_near_count = models.PositiveSmallIntegerField(
        null=True, blank=True, default=0
    )
    recent_measurements_excluded_few_count = models.PositiveSmallIntegerField(
        null=True, blank=True, default=0
    )
    recent_measurements_excluded_old_count = models.PositiveSmallIntegerField(
        null=True, blank=True, default=0
    )
    time_to_report_half_network = models.PositiveIntegerField(
        null=True, blank=True
    )
    version = models.CharField(
        max_length=16, null=True, blank=True, default=BWFILE_VERSION
    )

    # useful to understand calculations
    _mu = models.FloatField(null=True, blank=True)
    _muf = models.FloatField(null=True, blank=True)
    _bw_sum = models.PositiveBigIntegerField(null=True, blank=True)
    _bw_scaled_sum = models.PositiveBigIntegerField(null=True, blank=True)
    _limit = models.PositiveBigIntegerField(null=True, blank=True)
    _bw_scaled_limited_sum = models.PositiveBigIntegerField(
        null=True, blank=True
    )
    _bw_scaled_limited_rounded_sum = models.PositiveBigIntegerField(
        null=True, blank=True
    )
    # To store whether or not the minimum percent of relays to report has been
    # reached.
    _heartbeat = models.ForeignKey(
        "Heartbeat", null=True, blank=True, on_delete=models.SET_NULL
    )

    def set_mu(self):
        # NOTE: In the case all measurements fail, mu() will be None,
        # then set 1 to avoid division by 0.
        self._mu = Measurement.objects.mu() or 1
        self.save()
        logger.info("Network bandwidth average: %s", self._mu)

    def set_muf(self):
        # NOTE: In the case all measurements fail, muf() will be None,
        # then set 1 to avoid division by 0.
        self._muf = Measurement.objects.muf() or 1
        self.save()
        logger.info("Network bandwidth filtered average: %s", self._muf)

    # Just for statistics
    def set_bw_sum(self):
        self._bw_sum = self.relaybw_set.aggregate(models.Sum("bw_mean"))[
            "bw_mean__sum"
        ]
        self.save()
        return self._bw_sum

    def set_bw_scaled_sum(self):
        # No need to filter by relays to vote, since their bw scaled will be
        # null.
        self._bw_scaled_sum = (
            self.relaybw_set.aggregate(models.Sum("_bw_scaled"))[
                "_bw_scaled__sum"
            ]
            or 0
        )
        self.save()
        return self._bw_scaled_sum

    # Just for statistics
    def set_bw_scaled_limited_sum(self):
        self._bw_scaled_limited_sum = self.relaybw_set.aggregate(
            models.Sum("_bw_scaled_limited")
        )["_bw_scaled_limited__sum"]
        self.save()
        return self._bw_scaled_limited_sum

    def set_bw_scaled_limited_rounded_sum(self):
        self._bw_scaled_limited_rounded_sum = self.relaybw_set.aggregate(
            models.Sum("_bw_scaled_limited_rounded")
        )["_bw_scaled_limited_rounded__sum"]
        self.save()
        return self._bw_scaled_limited_rounded_sum

    def consensus_weight_percent_diff(self):
        """
        Return the percentage difference between the consensus weight sum and
        this Bandwidth file weight sum.

        """
        if self._bw_scaled_limited_rounded_sum is None:
            logger.warning(
                "Bandwidth scaled, limited and rounded sum is None."
                "Can not calculate the percentage difference with the "
                "consensus weight."
            )
            return None
        percent_diff = (
            abs(
                self.consensus.weight_sum()
                - self._bw_scaled_limited_rounded_sum
            )
            # Avoid ZeroDivisionError using the maximum between 1 and the sum.
            / (
                max(
                    1,
                    (
                        self.consensus.weight_sum()
                        + self._bw_scaled_limited_rounded_sum
                    ),
                )
                / 2
            )
        ) * 100
        logger.info(
            "The percentage difference between the consensus weight sum (%s) "
            "and this Bandwidth File weight sum (%s) is %s%%.",
            self.consensus.weight_sum(),
            self._bw_scaled_limited_rounded_sum,
            round(percent_diff),
        )
        return percent_diff

    def is_consensus_weight_percent_diff_greater_maximum(self):
        """
        Return True and log a warning if the percentage difference between the
        consensus weight sum and this Bandwidth File weight sum is greater than
        a maximum.

        Avoids sbws#40115.

        """
        percent_diff = self.consensus_weight_percent_diff()
        if (
            percent_diff is not None
            and percent_diff > config.MAX_WEIGHT_PERCENT_DIFF
        ):
            logger.warning(
                "The percentage difference between the consensus weight sum "
                "and this Bandwidth File weight sum is more than %s%%",
                config.MAX_WEIGHT_PERCENT_DIFF,
            )
            return True
        return False

    def estimated_time(self):
        elapsed_time = Scanner.load().set_elapsed_time()
        if elapsed_time:
            return round(
                (
                    elapsed_time.total_seconds()
                    * (self.consensus.relays_count() / 2)
                )
                / (self.eligible_relays() or 1)
            )
        return None

    def eligible_relays(self):
        return self.relaybw_set.exclude(vote=False).exclude(vote=0).count()

    def is_min_percent_relays_to_report(self):
        """
        Return True if the minimum percentage of measured relays with respect
        to the consensus has been reached and False otherwise.

        """
        reached = (
            self.number_eligible_relays >= self.minimum_number_eligible_relays
        )
        logger.debug(
            "The minimum percentage of measured relays to report their "
            "weight is reached: %s.",
            reached,
        )
        return reached

    def is_min_percent_relays_to_report_reached_before(self):
        """
        Return True if the minimum percentage of measured relays with respect
        to the consensus was reached before and False otherwise.

        """
        reached_before = (
            self._heartbeat._min_percent_relays_to_report_reached_at
        )
        logger.debug(
            "The minimum percentage of measured relays to report their "
            "weight war reached before: %s.",
            reached_before,
        )
        return reached_before

    def set_min_percent_relays_to_report_reached(self):
        heartbeat = self._heartbeat
        heartbeat._min_percent_relays_to_report_reached_at = (
            datetime.datetime.utcnow()
        )
        heartbeat.save()

    def calculate_progress_to_report(self):
        reached = self.is_min_percent_relays_to_report()
        reached_before = self.is_min_percent_relays_to_report_reached_before()
        if not reached:
            self.set_relaybws_under_min_to_report()
            # If the min percent was reached before, but is not reached now.
            if reached_before:
                logger.warning(
                    "The percentage of the measured relays (%s) is less than "
                    "the %s%% of the relays in the network (%s).",
                    self.number_eligible_relays,
                    self.minimum_percent_eligible_relays,
                    self.number_consensus_relays,
                )
        else:
            # The min percent was not reached before, but it's reached now.
            if not reached_before:
                logger.info(
                    "The percentage of the measured relays (%s) is more than "
                    "the %s%% of the relays in the network (%s)",
                    self.number_eligible_relays,
                    self.minimum_percent_eligible_relays,
                    self.number_consensus_relays,
                )
                self.set_min_percent_relays_to_report_reached()

    def set_relaybws_under_min_to_report(self):
        """
        Modify the Bandwidth Lines adding the KeyValue `under_min_report`,
        `vote`.

        Future version: this should probably be eliminated. It only has
        happened when the scanner has been running for very few days.

        """
        # Note that all the Bandwidth Lines are set, not only measured and
        # scaled ones.
        logger.debug(
            "Setting `under_min_report` to %s lines.", self.relaybw_set_count()
        )
        for relaybw in self.relaybw_set.all():
            relaybw.set_under_min_report()

    def write(self, bwfile_path=None):
        import os

        os.makedirs(config.BWFILE_DIR, exist_ok=True)
        path = os.path.join(
            config.BWFILE_DIR,
            self.file_created.strftime(constants.DATETIME_FORMAT) + ".txt",
        )
        with open(path, "w") as fd:
            fd.write(self.to_str())
        logger.debug("Written %s.", path)
        # To atomically symlink a file, we need to create a temporary link,
        # then rename it to the final link name. (POSIX guarantees that
        # rename is atomic.)
        tmp_path = os.path.join(config.BWFILE_DIR, config.BWFILE_PATH + ".tmp")
        os.symlink(path, tmp_path)
        logger.debug("Symlinked %s to %s.", path, tmp_path)
        bwfile_path = bwfile_path or config.BWFILE_PATH
        os.rename(tmp_path, bwfile_path)
        logger.debug("Renamed %s to %s.", tmp_path, bwfile_path)


@receiver(post_save, sender=BwFile(), dispatch_uid="delete_old")
def delete_old(sender, instance, **kwargs):
    old = datetime.datetime.utcnow() - datetime.timedelta(
        days=config.OLDEST_DATA_DAYS
    )
    for obj in sender.objects.filter(file_created__lt=old):
        logger.debug("Deleting old BwFile %s", obj)
        obj.delete()
