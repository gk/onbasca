# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import logging

from django.db import models

from .relay import Relay
from .webserver import WebServer

logger = logging.getLogger(__name__)


class MeasurementManager(models.Manager):
    def mu(self):
        # Same as:
        # relays = (
        #     Measurement.objects.order_by()
        #     .values_list("relay", flat=True)
        #     .distinct()
        # )
        # means = []
        # for relay in relays:
        #     means.append(
        #         Relay.objects.get(fingerprint=relay).bw_mean
        #     )
        # means = list(filter(lambda x: x is not None, means))
        # from statistics import mean
        # mean(means)
        # or self.values("relay__bw_mean")?, doesn't work with properties
        return (
            self.values("relay")
            .annotate(relay_bw_mean=models.Avg("bandwidth"))
            .aggregate(models.Avg("relay_bw_mean"))["relay_bw_mean__avg"]
        )

    def muf(self):
        return (
            self.values("relay")
            .annotate(relay_bw_mean=models.Avg("bandwidth"))
            .filter(bandwidth__gte=models.F("relay_bw_mean"))
            .aggregate(models.Avg("relay_bw_mean"))["relay_bw_mean__avg"]
        )


class Measurement(models.Model):
    class Meta:
        get_latest_by = "created_at"
        unique_together = ("consensus", "relay")

    _obj_created_at = models.DateTimeField(auto_now_add=True, null=True)
    _obj_updated_at = models.DateTimeField(auto_now=True, null=True)

    objects = MeasurementManager()

    consensus = models.ForeignKey(
        "Consensus", null=True, blank=True, on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(auto_now_add=True)
    queued_at = models.DateTimeField(blank=True, null=True)
    attempted_at = models.DateTimeField(blank=True, null=True)
    finished_at = models.DateTimeField(blank=True, null=True)
    relay = models.ForeignKey(
        Relay,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="measurements",
    )
    webserver = models.ForeignKey(
        WebServer, on_delete=models.CASCADE, blank=True, null=True
    )
    helper = models.ForeignKey(
        Relay,
        related_name="helper",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    as_exit = models.BooleanField(null=True, blank=True)
    bandwidth = models.PositiveIntegerField(blank=True, null=True)
    error = models.TextField(blank=True, null=True)

    def __str__(self):
        return "{}, {}".format(self.relay, self.error or str(self.bandwidth))
