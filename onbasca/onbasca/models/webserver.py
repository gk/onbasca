# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import logging
import random

from django.db import models

from onbasca.onbasca import config

logger = logging.getLogger(__name__)


class WebServerManager(models.Manager):
    def select_random(self):
        return random.SystemRandom().choice(
            WebServer.objects.filter(enabled=True)
        )

    def update_from_dict(self, webservers_dict):
        current_urls = []
        # Create or update new or existing Web servers
        for webserver_dict in webservers_dict:
            url = webserver_dict.pop("url", None)
            webserver, created = self.update_or_create(
                url=url, defaults=webserver_dict
            )
            current_urls.append(webserver.url)
            logger.debug("Web server: %s, created: %s.", webserver, created)
        # Delete existing ones that are not in the configuration
        for webserver in self.exclude(url__in=current_urls):
            logger.debug("Deleting Web server %s", webserver)
            webserver.delete()


# TODO: enable, disable, next
class WebServer(models.Model):
    _obj_created_at = models.DateTimeField(auto_now_add=True, null=True)
    _obj_updated_at = models.DateTimeField(auto_now=True, null=True)

    objects = WebServerManager()

    url = models.URLField(
        unique=True, max_length=255, default=config.WEB_SERVERS[0]["url"]
    )
    enabled = models.BooleanField(null=True, blank=True, default=True)
    verify = models.CharField(
        max_length=255, default=config.WEB_SERVERS[0]["verify"]
    )

    def __str__(self):
        return "{}, enabled: {}, verify: {}.".format(
            self.url, self.enabled, self.verify
        )
