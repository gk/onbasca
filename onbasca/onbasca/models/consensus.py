# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import datetime
import logging

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from onbasca.base.models.consensus import ConsensusBase, ConsensusManagerBase
from onbasca.onbasca import config

from .routerstatus import RouterStatus

logger = logging.getLogger(__name__)


class ConsensusManager(ConsensusManagerBase):
    def from_router_statuses(
        self, router_statuses, valid_after=None, testing_network=False
    ):
        consensus = super().from_router_statuses(router_statuses, valid_after)
        logger.info("Creating router statuses.")
        for rs in router_statuses:
            RouterStatus.objects.from_router_status(rs, consensus)
        logger.info("%s Router statuses created", len(router_statuses))
        # after the relays has been created
        return self.set_exits_non_exits(consensus, testing_network)

    def set_exits_non_exits(self, consensus, testing_network=False):
        # Set first the non exits min bw, so that if there aren't exits (what
        # can happen in chutney), we can take it as the min bw for exits too.
        consensus.set_non_exits_fingerprints(testing_network)
        consensus.set_non_exits_min_position()
        consensus.set_non_exits_min_bandwidth()

        consensus.set_exits_fingerprints(testing_network)
        consensus.set_exits_min_position()
        consensus.set_exits_min_bandwidth()
        consensus.save()
        return consensus


class Consensus(ConsensusBase):
    class Meta:
        get_latest_by = "valid_after"

    objects = ConsensusManager()

    _exits_min_bandwidth = models.PositiveIntegerField(null=True, blank=True)
    _exits_min_position = models.PositiveSmallIntegerField(
        null=True, blank=True
    )

    _non_exits_min_bandwidth = models.PositiveIntegerField(
        null=True, blank=True
    )
    _non_exits_min_position = models.PositiveSmallIntegerField(
        null=True, blank=True
    )

    # for scanner
    def routerstatuses_ordered(self):
        """

        https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/34393:
        Measure first relays without measurements.

        """
        routerstatuses_ordered = self.routerstatus_set.order_by(
            "_measurement_latest__created_at"
        )
        logger.debug(
            "Number of ordered RouterStatuses: %s",
            routerstatuses_ordered.count(),
        )
        return routerstatuses_ordered

    def exits(self):
        return self.routerstatus_set.filter(is_exit=True)

    def non_exits(self):
        return self.routerstatus_set.filter(is_exit=False)

    def set_exits_fingerprints(self, testing_network=False):
        """Create a list with exits fingerprints.

        To later calculate the minimum bandwidth the exits must have.

        """
        self._exits_fingerprints = (
            self.relay_set.values_list("fingerprint", flat=True)
            .filter(relaydesc__can_exit_443=True)
            .filter(routerstatus__is_exit=True)
            .distinct()
        )
        # Testing network:
        if not self._exits_fingerprints and testing_network:
            self._exits_fingerprints = self.routerstatus_set.all().values_list(
                "fingerprint", flat=True
            )
            logger.debug(
                "Testing network, assuming all relays are exits.",
            )
            self._exits_fingerprints = self.routerstatus_set.all().values_list(
                "fingerprint", flat=True
            )
        logger.debug(
            "Number of relays that are exits and can exit 443 %s",
            self._exits_fingerprints.count(),
        )
        self.save()
        return self._exits_fingerprints

    def set_exits_min_position(self):
        """Calculate 1/4 of the exits and and pick that position.

        To later calculate and set what's the lowest bandwidth for an exit,
        ignoring the 25% with lower bandwidth.

        """
        self._exits_min_position = int(self._exits_fingerprints.count() / 4)
        logger.debug(
            "Minimum position for exits: %s", self._exits_min_position
        )
        self.save()
        return self._exits_min_position

    def set_exits_min_bandwidth(self):
        """Calculate and set the minimum bandwidth for exits.

        Ordering the exits by bandwidth, obtain the lowest bandwidth after
        ignoring the first 25% percent of slower exits.

        Avoids https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/33009  # noqa

        """
        exits_min_bandwidth = (
            self.routerstatus_set.filter(
                fingerprint__in=self._exits_fingerprints
            )
            .order_by("bandwidth")
            .values_list("bandwidth", flat=True)
        )
        # In chutney it can happen that there are not exits
        if exits_min_bandwidth:
            self._exits_min_bandwidth = exits_min_bandwidth[
                self._exits_min_position
            ]
        # Take min bandwidth from non exits.
        # NOTE that `set_non_exits_min_bandwidth` needs to be executed first.
        else:
            logger.debug("There are no exits, setting min bw from non exits.")
            self._exits_min_bandwidth = self._non_exits_min_bandwidth
        logger.debug("Min bandwidth for exits: %s", self._exits_min_bandwidth)
        self.save()
        return self._exits_min_bandwidth

    def exits_with_bandwidth_fingerprints(self, routerstatus):
        """Return the exits with bandwidth greater than minimum.

        After calculating the lowest bandwidth for exits.

        """
        bandwidth = max(routerstatus.bandwidth, self._exits_min_bandwidth)
        exits = (
            self.routerstatus_set.filter(bandwidth__gte=bandwidth)
            .exclude(fingerprint=routerstatus.fingerprint)
            .values_list("fingerprint", flat=True)
        )
        logger.debug(
            "Number of exits with bandwidth greater than %s: %s",
            bandwidth,
            exits.count(),
        )
        return exits

    def set_non_exits_fingerprints(self, testing_network=False):
        """Create a list with non exits fingerprints.

        To later calculate the minimum bandwidth the non exits must have.

        """
        self._non_exits_fingerprints = (
            self.relay_set.values_list("fingerprint", flat=True)
            .filter(relaydesc__can_exit_443=False)
            .filter(routerstatus__is_exit=False)
            .distinct()
        )
        # Testing network:
        if not self._non_exits_fingerprints and testing_network:
            self._non_exits_fingerprints = (
                self.routerstatus_set.all().values_list(
                    "fingerprint", flat=True
                )
            )
            logger.debug(
                "Testing network, assuming all relays are non exits.",
            )
        logger.debug(
            "Number of relays that are not exits and can not exit 443 %s",
            self._non_exits_fingerprints.count(),
        )
        return self._non_exits_fingerprints

    def set_non_exits_min_position(self):
        """Calculate 1/4 of the non exits and pick that position.

        To later calculate and set what's the lowest bandwidth for an non exit,
        ignoring the 25% with lower bandwidth.

        """
        self._non_exits_min_position = int(
            self._non_exits_fingerprints.count() / 4
        )
        logger.debug(
            "Minimum position for non exits: %s", self._non_exits_min_position
        )
        self.save()
        return self._non_exits_min_position

    def set_non_exits_min_bandwidth(self):
        """Calculate and set the minimum bandwidth for non exits.

        Ordering the non exits by bandwidth, obtain the lowest bandwidth after
        ignoring the first 25% percent of slower non exits.

        Avoids https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/33009  # noqa

        """
        non_exits = (
            self.routerstatus_set.values_list("bandwidth", flat=True)
            .filter(fingerprint__in=self._non_exits_fingerprints)
            .order_by("bandwidth")
        )
        if non_exits:
            self._non_exits_min_bandwidth = non_exits[
                self._non_exits_min_position
            ]
        else:
            logger.warning("There are not non exits.")
            self._non_exits_min_bandwidth = 0
        logger.debug(
            "Min bandwidth for non exits: %s", self._non_exits_min_bandwidth
        )
        self.save()
        return self._non_exits_min_bandwidth

    def non_exits_with_bandwidth_fingerprints(self, routerstatus):
        """Return the non exits with bandwidth greater than minimum.

        After calculating the lowest bandwidth for non exits.

        """
        # Even in a testing network without non exits, _non_exits_min_bandwidth
        # should have been set by :func:`set_non_exits_min_bandwidth`
        bandwidth = max(self._non_exits_min_bandwidth, routerstatus.bandwidth)
        non_exits = (
            self.routerstatus_set.filter(bandwidth__gte=bandwidth)
            .exclude(fingerprint=routerstatus.fingerprint)
            .values_list("fingerprint", flat=True)
        )
        logger.debug(
            "Number of non exits with bandwidth greater than %s: %s",
            bandwidth,
            non_exits.count(),
        )
        return non_exits

    def bandwidth_sum(self):
        """Return consensus bandwidth sum."""
        return self.routerstatus_set.aggregate(models.Sum("bandwidth"))[
            "bandwidth__sum"
        ]

    def weight_sum(self):
        """Return consensus weight sum.

        This is the same as the ``bandwidth sum`` but divided by 1000, as the
        consensus publish weights.

        """
        return self.bandwidth_sum() / 1000


@receiver(post_save, sender=Consensus, dispatch_uid="delete_old")
def delete_old(sender, instance, **kwargs):
    old = datetime.datetime.utcnow() - datetime.timedelta(
        days=config.OLDEST_DATA_DAYS
    )
    for obj in sender.objects.filter(valid_after__lt=old):
        logger.debug("Deleting old Consensus %s", obj)
        obj.delete()
