# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from django.db import models


class BwAuth(models.Model):
    _obj_created_at = models.DateTimeField(auto_now_add=True)
    _obj_updated_at = models.DateTimeField(auto_now=True)
    nickname = models.CharField(max_length=19, null=True, blank=True)
    fingerprint = models.CharField(max_length=40, null=True, blank=True)
    # consensuses = models.ManyToManyField("Consensus")
    # software = models.CharField(max_length=64, null=True, blank=True)
    # ipv4 = models.GenericIPAddressField(null=True, blank=True)
    # contact = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.nickname or ""

    # def recent_consensus_count(self):
    #     return self.consensuses.count()
