# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import logging

logger = logging.getLogger(__name__)


def modify_logging(scan=True, generate=False, log_level=None):
    import os
    from copy import deepcopy

    from django.conf import settings

    from . import config

    custom_logging = deepcopy(settings.LOGGING)
    if scan:
        custom_logging["handlers"]["file"]["filename"] = config.SCAN_LOG_PATH
    elif generate:
        custom_logging["handlers"]["file"][
            "filename"
        ] = config.GENERATE_LOG_PATH
    else:
        custom_logging["handlers"]["file"]["filename"] = config.CLEAN_LOG_PATH
    logger.info(
        "Modifying logging with file handler path: %s",
        custom_logging["handlers"]["file"]["filename"],
    )
    os.makedirs(config.LOGS_DIR, mode=0o750, exist_ok=True)
    if log_level:
        custom_logging["loggers"]["onbasca"]["level"] = log_level.upper()
    logging.config.dictConfig(custom_logging)
