# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from django.db import models
from stem import Flag

from onbasca.base import constants

from . import BaseModel


class RouterStatusManagerBase(models.Manager):
    def from_router_status(self, router_status, consensus=None, **kwargs):
        kwargs.update(
            {
                "published": router_status.published,
                "nickname": router_status.nickname,
                "address": router_status.address,
                "bandwidth": router_status.bandwidth * constants.KB,
                "is_unmeasured": router_status.is_unmeasured,
                "is_exit": Flag.BADEXIT not in router_status.flags
                and Flag.EXIT in router_status.flags,
            }
        )
        kwargs = dict(filter(lambda kv: kv[1] is not None, kwargs.items()))
        # if not consensus:
        #     router_status
        kwargs["consensus"] = consensus
        rs, _ = self.update_or_create(
            fingerprint=router_status.fingerprint,
            consensus=consensus,
            defaults=kwargs,
        )
        return rs


class RouterStatusBase(BaseModel):
    class Meta:
        abstract = True
        get_latest_by = "published"
        unique_together = ("consensus", "fingerprint")

    objects = RouterStatusManagerBase()
    fingerprint = models.CharField(max_length=40)
    # Relay and Consensus ForeignKey is not defined here, but in non abstract
    # classes.
    published = models.DateTimeField(editable=True, null=True, blank=True)
    # XXX: length
    nickname = models.CharField(max_length=19, null=True, blank=True)
    address = models.CharField(max_length=255, null=True, blank=True)
    bandwidth = models.PositiveIntegerField(null=True, blank=True)
    is_unmeasured = models.BooleanField(null=True, blank=True)
    is_exit = models.BooleanField(null=True, blank=True)

    def __str__(self):
        return str(self.relay)
