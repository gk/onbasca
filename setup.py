# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""The setup script."""
import setuptools
import versioneer

setuptools.setup(
    version=versioneer.get_version(), cmdclass=versioneer.get_cmdclass()
)
