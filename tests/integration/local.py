# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

# Import either production or development with `*`, so that django settings
# read all contanstatns
from .development import *  # noqa: F401, F403

print("Loading local.py")
from django.core.management.utils import get_random_secret_key  # noqa: E402

print("Generating random key on each run. You can write it down in this file.")
SECRET_KEY = get_random_secret_key()

ALLOWED_HOSTS = ["*"]

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "mydb",
        "USER": "myuser",
        "PASSWORD": "mypass",
        "HOST": "localhost",
        "PORT": "",
    }
}
print(DATABASES["default"])
