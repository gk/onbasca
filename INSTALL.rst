.. SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
..
.. SPDX-License-Identifier: CC0-1.0

.. _install:

Installation
============

System software dependencies
----------------------------

- [tor]_ (last stable version is recommended)
- [PostgreSQL]_ ([libpq-dev]_)
- [Python3]_ (>= 3.6)

Python dependencies
--------------------

These dependencies might be unupdated. The list of updated dependencies is in
the [setup.cfg]_ file.

- [Django3]_
- [Psycopg2]_
- [stem]_ >= 1.8.0
- [requests]_ (with [socks]_ support) >= 2.10.0
- [toml]_ >= 0.10

It is recommend to install the dependencies from your system package manager.

Installation from source
------------------------

Clone ``onbasca``::

    git clone https://gitlab.torproject.org/tpo/network-health/onbasca.git
    git checkout $(git describe --abbrev=0 --tags)

(``git describe --abbrev=0 --tags`` provides the latest stable version which
should be used in production)

install the dependencies and packages::

    cd onbasca
    python3 setup.py install

If you're installing ``onbasca`` for development or testing, it's recommend to
install the dependencies and packages in a [virtualenv]_.

Create the database
-------------------

In a Debian system::

    sudo su postgres
    psql
    create user <username> with password '<password>';
    create database <dbname>;
    grant all privileges on database <dbname> to <username>;

Create the scheme::

    ./manage.py makemigrations onbasca
    ./manage.py migrate

Run the scanner and the generator for development
-------------------------------------------------

To run the scanner and the generator::

    ./manage.py scan &
    ./manage.py generate &

Run the scanner and the generator for production
-------------------------------------------------

See `<DEPLOY.rst>`_ (in the local directory or Tor Project Gitlab)
or `<DEPLOY.html>`_ (local build).
