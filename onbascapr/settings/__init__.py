# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

print("Loading onbascapr/settings/__init__.py/.")
# local (or production or development) in that order will import the others.
try:
    from .local import *
except ImportError as e:
    print("import error", e)
    try:
        from .production import *
    except ImportError:
        from .development import *

# Or import all here, then there is no need to import in each of these files.
# try:
#     from .local import *
# except ImportError:
#     pass
# try:
#     from .production import *
# except ImportError:
#     from .development import *
# from onbasca.onbasca.defaults import *
