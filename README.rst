.. SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
..
.. SPDX-License-Identifier: CC0-1.0

README
========

Onion Bandwidth Scanner (onbasca) is a bandwidth scanner implementation using
a database via an [Object-relationalMapping]_ (ORM) framework.

The scanner measures the bandwidth of each relay in the Tor network
(except the directory authorities) by creating a two hops circuit with
the relay. It then measures the bandwidth by downloading data from a
Web Server and stores the measurements.

The generator read the measurements, aggregates, filters and scales them
using [Torflow]_’s scaling method. Then it generates a [BandwidthFile]_
that is read by a directory authority to report relays’ bandwidth in its
vote.

.. warning::
   This software is intended to be run by researchers using a
   test Tor network, such as [Chutney]_ or [Shadow]_, or by the Tor bandwidth
   authorities on the public Tor network. Please do not run this software
   on the public Tor network unless you are one of the Tor bandwidth
   authorities, to avoid creating unnecessary traffic.

This [blog_post]_ also explains how Tor bandwidth scanners work.

.. hint:: It is recommended to read this documentation at
   https://tpo.pages.torproject.net/network-health/onbasca.
   At https://gitlab.torproject.org/tpo/network-health/onbasca.git
   some links won’t be properly rendered. .

Installation
------------

See `./INSTALL.rst <INSTALL.rst>`__ (in local directory or tpo Gitlab)
or `INSTALL.html <INSTALL.html>`__ (local build).

Deployment
----------

See `./DEPLOY.rst <DEPLOY.rst>`__ (in local directory or tpo Gitlab) or
`DEPLOY.html <DEPLOY.html>`__ (local build).

Documentation
-------------

More extensive documentation can be found in the ``./docs`` directory,
and online at https://tpo.pages.torproject.net/network-health/onbasca.

License
-------

Copyright (c) 2020-2021, juga at riseup dot net.
Copyright (c) 2022, The Tor Project, Inc.

Released under the BSD-3-Clause, which applies to all the source code and
binary files except otherwise stated.

Documentation and other files are released under CC0-1.0 license.
